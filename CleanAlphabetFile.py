import os
import sys

from PIL import Image

RED = (255,0,0,255)
GREEN = (0,255,0,255)
WHITE = (255,255,255,255)

if sys.argv and len(sys.argv) < 2:
    print("Please provide the filename.")
    sys.exit(0)
    
filename = sys.argv[1]
if not os.path.isfile(filename) or not filename.endswith(".png"):
    print(f"Cannot use {filename}.\nPlease provide a valid png file.")
    sys.exit(0)

my_image = Image.open(filename)
width,height = my_image.size

px_colour = None
for x in range(width):
    for y in range(height):
        px_colour = my_image.getpixel((x,y))
        if px_colour == RED or px_colour == GREEN:
            my_image.putpixel((x,y),WHITE)

my_image.save(filename)