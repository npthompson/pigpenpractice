import wx
import random
from math import floor

from datetime import datetime

class MissingAssetException(BaseException):
    ''' This exception gets thrown if an asset is missing. '''
    def __init__(self,reason):
        super().__init__(self,reason)


class WordGenerator():
    
    ALPHABET = [chr(x + ord('a')) for x in range(26)]
    MAX_WORD_LENGTH = 14
    WHITE = wx.Colour(255,255,255)
    
    def __init__(self):
        random.seed(datetime.now().second)
        self.alphabet = self.generatealphabet()
        self.wordlist = list(set("the quick brown fox jumped over the lazy dog".split(" ")))
        self.lastletter = None
        self.lastword = None
        self.alphabetimage = wx.Bitmap()
        try:
            with open("wordlist.txt","r") as infile:
                self.wordlist = infile.readlines()
            self.wordlist = [w.strip() for w in self.wordlist if not w.startswith("#")] # ignore comment lines.
        except:
            raise MissingAssetException("Missing wordlist.txt asset.")
        
        try:
            self.alphabetimage.LoadFile("alphabet.png")
        except:
            raise MissingAssetException("Missing alphabet.png asset.")
       
    
    def generatealphabet(self):
        ''' Generates a shuffled alphabet. Returns a list of chars. '''
        alphabet = list.copy(WordGenerator.ALPHABET)
        random.shuffle(alphabet)        
        return alphabet
        
    def getletter(self,letter):
        letter = letter[0]
        letter = letter.lower()
        index = ord(letter) - ord('a')
        if index < 0 or index > 26:
            raise ValueError(f"The letter {letter} (ordinal {ord(letter)}) is not in the available alphabet.")
        x = 28*index
        y = 0
        
        try:
            result = self.alphabetimage.GetSubBitmap(wx.Rect(x,y,28,28))
        except:
            print(f"Error getting letter {letter}, ord {ord(letter)}")
        return result

    def getword(self,word):
        if not isinstance(word,str):
            raise ValueError(f"WordGenerator.getword() requires a string. received a {type(word)} instead.")

        width = len(word)*28
        height = 28
        canvas = wx.Bitmap()
        canvas.Create(width,height,depth=32)
        paint = wx.MemoryDC()
        paint.SelectObject(canvas)
        
        for pos,letter in enumerate(word):
            paint.DrawBitmap(self.getletter(letter),28*pos,0)
        
        return canvas

    def standardsize(self,bitmap):
        my_width = 28*WordGenerator.MAX_WORD_LENGTH
        my_height = 28
        
        canvas = wx.Bitmap()
        canvas.Create(my_width,my_height,depth=32)
        paint = wx.MemoryDC()
        paint.SelectObject(canvas)
        
        paint.SetPen(wx.WHITE_PEN)
        paint.SetBrush(wx.WHITE_BRUSH)
        paint.DrawRectangle(0,0,my_width,my_height)
        
        x_insert_pos = floor((my_width - bitmap.GetSize()[0])/ 2)
        paint.DrawBitmap(bitmap,x_insert_pos,0)
        
        return canvas
        

    def getrandomletter(self):
        # make sure there are letters in our alphabet.
        if len(self.alphabet) == 0:
            self.alphabet = self.generatealphabet()
            
        # get a random letter.
        letter = self.alphabet.pop()
            
        #if it's the same letter we gave out last time, get a new one.
        if self.lastletter == letter:
            return self.getrandomletter()
        else: # otherwise, give theletter we have and record it so we don't send it out again.
            self.lastletter = letter
            return self.getletter(letter),letter
        
    def getrandomword(self):
        word = random.choice(self.wordlist)
        if self.lastword == word:
            return self.getrandomword()
        else:
            self.lastword = word
            return self.getword(word),word
        