from PIL import Image

RED = (255,0,0,255)
GREEN = (0,255,0,255)
WHITE = (255,255,255,255)

my_image = Image.new("RGBA",(28*26,28),WHITE)

def draw_bar(image,x,color):
    width,height = image.size
    #print(f"{width}x{height}: {x}")
    if x < 0 or x > width:
        return
    for i in range(height):
        #print(i)
        image.putpixel((x,i),color)


#draw left borders
for i in range(0,28*26,28):
    draw_bar(my_image,i,GREEN)

#draw right borders
for i in range(27,28*26,28):
    draw_bar(my_image,i,RED)
    
my_image.save("blank.png")

