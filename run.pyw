import sys

import wx

from UserInterface import UserInterface

if __name__ == "__main__":
    # define some variables used below.
    title = "PigPen Practice"

    # Print help.
    if set(sys.argv) & {'help','--help','-h','--h'}:
        app_name = sys.argv[0]
        if os.path.sep in app_name:
            app_name = app_name[app_name.rfind(os.path.sep)+1:]
        print(f"{app_name}\n",
              "-h\tPrints this help message\n",
              "-t\tSets a custom window title.\n")
        sys.exit(0)
    
    # Window title.
    if '-t' in sys.argv:
        title_pos = sys.argv.index('-t') + 1 
        if len(sys.argv) > title_pos:
            title = sys.argv[title_pos]
        else:
            title = ""
            
    # launch the app.        
    app = wx.App()
    main_window = UserInterface(None,title=title)
    app.MainLoop()    