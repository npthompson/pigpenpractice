import os
import sys
from datetime import datetime

import wx

from WordGenerator import WordGenerator, MissingAssetException

class UserInterface(wx.Frame):
    ''' user interface for pigpenpractice. '''
    
    GUESSHISTORY = 26
    LETTERMODE = 1
    WORDMODE = 2
    
    def __init__(self,parent,title = "PigPen Practice",*args,**kwargs):
        wx.Frame.__init__(self,parent, title=title,size=(400,400))
        
        self.wordgenerator = WordGenerator()
        
        # store the image to display, the answer as a string, and the starttime.
        self.word = self.wordgenerator.standardsize(self.wordgenerator.getletter('a'))
        self.answer, self.starttime =None,None
        self.mode = UserInterface.LETTERMODE
        
        #statistics
        self.guesstimer = list()
        self.mistakes = 0
        
        self.statusbar = self.CreateStatusBar(3)
        self.statusbar.SetStatusWidths([-1,-2,-1])
        
        self.panel = wx.Panel(self)
        self.vertical_sizer = wx.BoxSizer(wx.VERTICAL)
        
        toolbar_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.vertical_sizer.Add(toolbar_sizer)
        
        self.mode_button = wx.Button(self.panel,wx.ID_ANY,label="Change &Mode")
        self.Bind(wx.EVT_BUTTON,self.OnChangeModePressed,self.mode_button)
        toolbar_sizer.Add(self.mode_button,1,wx.ALL,3)
        
        toolbar_sizer.AddSpacer(1)
        
        self.reset_button = wx.Button(self.panel,wx.ID_ANY,label="Reset")
        self.Bind(wx.EVT_BUTTON,self.OnResetPressed,self.reset_button)
        toolbar_sizer.Add(self.reset_button,1,wx.ALL,3)
        
        self.vertical_sizer.AddSpacer(1)
        self.word_display = wx.StaticBitmap(self.panel,wx.ID_ANY,bitmap=self.word)
        self.vertical_sizer.Add(self.word_display,3,wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL,3)
        
        self.vertical_sizer.Add(wx.StaticText(self.panel,wx.ID_ANY,label="Answer:"),0,wx.ALL,3)
        
        self.answer_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.answer_box = wx.TextCtrl(self.panel,wx.ID_ANY,style=wx.EXPAND|wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER,self.OnKeyPressed,self.answer_box)
        self.submit_button = wx.Button(self.panel,wx.ID_ANY,label="submit")
        self.Bind(wx.EVT_BUTTON,self.OnSubmitAnswer,self.submit_button)
        self.answer_sizer.Add(self.answer_box,3,wx.EXPAND|wx.ALL,3)
        self.answer_sizer.Add(self.submit_button,0,0,3)
        self.vertical_sizer.Add(self.answer_sizer,0,wx.EXPAND|wx.ALL,3)
        
        self.panel.SetSizer(self.vertical_sizer)
        
        self.vertical_sizer.SetSizeHints(self)        
        self.SetMinSize((400,200))
        self.Show()
        self.Center()

        self.answer_box.SetFocus()
        self.reset()
    
    def reset(self):
        ''' Get a new word or letter, reset the statistics, and update the status. '''
        if self.mode == UserInterface.LETTERMODE:
            self.new_letter()
        else:
            self.new_word()        
        self.guesstimer = list()
        self.mistakes = 0
        self.update_status()
    
    
    def tick_starttime(self,word_size = 1):
        if self.starttime:
            time = datetime.now() - self.starttime
            seconds = time.total_seconds()
            seconds /= word_size
            self.guesstimer.append(seconds)
            if len(self.guesstimer) > UserInterface.GUESSHISTORY:
                self.guesstimer = self.guesstimer[-UserInterface.GUESSHISTORY:]
        self.starttime = datetime.now()
    
    def new_letter(self):
        ''' Get a new letter for the user to guess. '''
        self.word, self.answer = self.wordgenerator.getrandomletter()
        self.word = self.wordgenerator.standardsize(self.word)
        self.tick_starttime()
        self.word_display.SetBitmap(self.word)
        self.panel.Refresh()

    def new_word(self):
        ''' Get a new word for the user to guess. ''' 
        self.word, self.answer = self.wordgenerator.getrandomword()
        self.word = self.wordgenerator.standardsize(self.word)
        self.tick_starttime(len(self.answer))
        self.word_display.SetBitmap(self.word)
        self.panel.Refresh()
        
    def get_letter_average(self):
        ''' Update a display of the time for the last several guesses. '''
        guesscount = len(self.guesstimer)
        averageseconds = None
        if guesscount > 0:
            averageseconds = f"{sum(self.guesstimer) / guesscount:.2f}"
        return guesscount,averageseconds
        
    
    def update_status(self):
        # Mode
        if self.mode == UserInterface.LETTERMODE:
            self.statusbar.SetStatusText("Letter Mode",0)
        else:
            self.statusbar.SetStatusText("Word Mode",0)
        
        #Averages
        count,av_seconds = self.get_letter_average()
        self.statusbar.SetStatusText(f"Avg time/letter {av_seconds}",1)
        
        # mistakes
        self.statusbar.SetStatusText(f"{self.mistakes} mistakes",2)
        
    
    def OnChangeModePressed(self,e):
        self.mode = UserInterface.WORDMODE if self.mode == UserInterface.LETTERMODE else UserInterface.LETTERMODE
        self.reset()
    
    def OnResetPressed(self,e):
        self.reset()
        
    def OnSubmitAnswer(self,e):
        answer = self.answer_box.Value.strip()
        if answer == "":
            return
        answer = answer.strip() # remove white space.
        if answer.lower() == self.answer.lower():
            if self.mode == UserInterface.WORDMODE:
                self.new_word()
            else:
                self.new_letter()
        else:
            self.mistakes+=1
            wx.Bell()
        self.answer_box.SetValue("")    
        self.update_status()

    def OnKeyPressed(self,e):
        self.OnSubmitAnswer(None)
